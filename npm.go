package main

import (
	"encoding/json"
	"io"
	"os"
	"os/exec"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

type npmPackageManager struct{}

func (pm npmPackageManager) name() issue.PackageManager {
	return issue.PackageManagerNpm
}

func (pm npmPackageManager) build(path string) error {
	cmd := exec.Command("npm", "install")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func (pm npmPackageManager) analyze(path string) ([]byte, error) {
	cmd := exec.Command("npm", "audit", "--json")
	cmd.Dir = path
	cmd.Env = os.Environ()
	return cmd.Output()
}

func (pm npmPackageManager) lockfile() string {
	return npmLockfile
}

func (pm npmPackageManager) prepare(reader io.Reader) (*AuditReport, error) {
	// decode output
	var result AuditReport
	err := json.NewDecoder(reader).Decode(&result)
	return &result, err
}
