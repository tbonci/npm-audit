package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/logrusorgru/aurora"
	"github.com/urfave/cli"
)

// PrependPath points to the path of the project under analysis
var PrependPath = ""

func pathExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	if canConnectToNpm() != nil {
		fmt.Println(aurora.Red("analyze: cannot connect to NPM"))
		// fail gracefully: return an empty string reader
		return ioutil.NopCloser(strings.NewReader("")), nil
	}

	PrependPath = path
	pm, err := detectPm(path)
	if err != nil {
		return nil, err
	}

	// npm audit and yarn audit require rely on a lockfile to be present. If none is give, we build the project.
	if pathExists(filepath.Join(path, pm.lockfile())) {
		fmt.Println("lockfile", pm.lockfile(), "found, no need to build the project.")
	} else {
		err = pm.build(path)
		if err != nil {
			return nil, err
		}
	}

	// we ignore the exit status as this points as npm audit will return with
	// an exit status of 1 if it found a violation
	out, _ := pm.analyze(path)

	if len(out) <= 0 {
		err = fmt.Errorf("analyze failed for %s", pm.name())
	}

	return ioutil.NopCloser(bytes.NewReader(out)), err
}
