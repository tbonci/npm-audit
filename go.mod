module gitlab.com/gitlab-org/secure/vulnerability-research/npm-audit

require (
	github.com/logrusorgru/aurora v0.0.0-20181002194514-a7b3b318ed4e
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.5.6
)

go 1.13
