package main

import (
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestParseSeverity(t *testing.T) {

	for _, tc := range []struct {
		slevel string
		ilevel issue.SeverityLevel
	}{
		// npm
		{"critical",
			issue.SeverityLevelCritical,
		},
		{"high",
			issue.SeverityLevelHigh,
		},
		{"moderate",
			issue.SeverityLevelMedium,
		},
		{"low",
			issue.SeverityLevelLow,
		},
		{"unknown",
			issue.SeverityLevelUnknown,
		},
		{"undef",
			issue.SeverityLevelUndefined,
		},
		// yarn
		{"info",
			issue.SeverityLevelInfo,
		},
		{"low",
			issue.SeverityLevelLow,
		},
		{"moderate",
			issue.SeverityLevelMedium,
		},
		{"High",
			issue.SeverityLevelHigh,
		},
		{"critical",
			issue.SeverityLevelCritical,
		},
	} {
		t.Run(t.Name(), func(t *testing.T) {
			level := parseSeverityLevel(tc.slevel)
			if level != tc.ilevel {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", tc.ilevel.String(), level.String())
			}
		})
	}
}
