package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match the proper analyzer
func Match(path string, info os.FileInfo) (bool, error) {
	switch info.Name() {
	case "package.json":
		return true, nil
	case "yarn.lock":
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("npm-audit", Match)
}
