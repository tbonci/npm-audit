# NPM/YARN audit analyzer

Dependency Scanning for projects that are using `yarn audit` or `npm audit`. This project wraps both the
[npm audit](https://docs.npmjs.com/cli/audit) and 
[yarn audit](https://classic.yarnpkg.com/en/docs/cli/audit/) analyzers which both rely on the NPM API.

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
