package main

import (
	"errors"
	"fmt"
	"net/http"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	scannerID   = "npm-audit"
	scannerName = "npm-audit"

	npmAPI       = "https://www.npmjs.com/advisories"
	yarnLockfile = "yarn.lock"
	npmDepFile   = "package.json"
	npmLockfile  = "package-lock.json"
)

// AuditReport captures the commonalities between NPM and Yarn audit reports
type AuditReport struct {
	Advisories map[string]Advisory
}

// Advisory represents a single advisory for both npm and yarn audit
type Advisory struct {
	Findings []struct {
		Version string
		Paths   []string
	}
	ID         int
	Title      string
	ModuleName string `json:"module_name"`
	Cves       []interface {
	}
	Severity string
	URL      string
}

func detectPm(path string) (pm, error) {

	depfileAvailable := func(path string, lockfile string) bool {
		return pathExists(filepath.Join(path, lockfile)) || pathExists(filepath.Join(PrependPath, lockfile))
	}

	if depfileAvailable(path, yarnLockfile) {
		return yarnPackageManager{}, nil
	} else if depfileAvailable(path, npmDepFile) {
		return npmPackageManager{}, nil
	} else {
		return nil, errors.New("No package manager identified")
	}
}

func canConnectToNpm() error {
	_, err := http.Get(npmAPI)
	if err != nil {
		return fmt.Errorf("cannot connect to NPM %w", err)
	}
	return nil
}

// see https://docs.npmjs.com/about-audit-reports#severity
// https://classic.yarnpkg.com/en/docs/cli/audit/
func parseSeverityLevel(s string) issue.SeverityLevel {
	switch strings.ToLower(s) {
	case "moderate":
		return issue.SeverityLevelMedium
	default:
		return issue.ParseSeverityLevel(s)
	}
}
