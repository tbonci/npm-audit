FROM node:11-alpine

# install git for project with git-sourced dependencies
RUN apk add --no-cache git build-base

# temporary workaround for
# https://github.com/nodejs/docker-node/issues/813#issuecomment-407339011
RUN npm config set unsafe-perm true
RUN npm install -g yarn

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
