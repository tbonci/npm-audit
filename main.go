package main

import (
	"log"
	"os"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/secure/vulnerability-research/npm-audit/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "(npm|yarn) audit analyzer for GitLab Dependency-Scanning"
	app.Author = "GitLab"

	app.Commands = command.NewCommands(command.Config{
		ArtifactName: command.ArtifactNameDependencyScanning,
		Match:        plugin.Match,
		Analyze:      analyze,
		Convert:      convert,
		AnalyzeFlags: analyzeFlags(),
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
